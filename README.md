Random Fourier Features [Rahimi&Recht 2007/2008] provide a nice way of scaling up Gaussian process regression.  This is a simple Stan implementation, using an exponentiated quadratic (aka squared exponential / Gaussian / RBF) kernel.

Author: [Seth Flaxman](www.sethrf.com)

License: public domain

![IMAGE](https://bytebucket.org/flaxter/random-fourier-features-in-stan/raw/1dd9625a38d3e79090fc8c0f9eb5532aea1fcb1b/figure.png)